package Figures;

import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JPanel;
import com.modeliosoft.modelio.javadesigner.annotations.objid;

@objid ("f43338a6-ba1b-4ed6-90ea-4699d8028d8c")
@SuppressWarnings("serial")
public class Paper extends JPanel implements MouseListener, KeyListener, Interfaces {
    @objid ("f377b5d2-032e-4a56-8e57-50c4b4c43746")
     static boolean rectangles = false;

    @objid ("5bcbdb77-d9d9-45fd-a3af-f9db55b9ca9a")
     static boolean circles = false;

    @objid ("b786bd6b-a97e-4b73-8750-52dd5a6a7a9d")
     static boolean lines = false;

    @objid ("69475215-2895-4611-b5d4-fa6fc39184f2")
    public static char form = 'l';

    @objid ("75b75dad-f322-4c32-9d5b-490762861fb7")
    private int x;

    @objid ("d2968eed-9c31-4a6a-98a5-31effedcffd7")
    private int y;

    @objid ("7dcc7f37-0198-4873-b35f-ce93c5513eff")
    private Drawing drawing;

    @objid ("332709e4-6d24-4446-ba90-2afa71733c96")
    public Paper() {
        addMouseListener(this);
    }

    @objid ("ddf69b06-3fe7-4790-a300-2bdaa6698598")
    char test(char form) {
        return 0;
    }

// 2 Sprachelemente
    @objid ("ae30c740-000d-4d30-8f77-fb3f0d82745b")
    @Override
    public void mousePressed(MouseEvent e) {
        x = e.getX();
        y = e.getY();
    }

    @objid ("cf4d75e0-e110-428b-808d-17f9b7c773fb")
    @Override
    public void mouseReleased(MouseEvent e) {
        int width = e.getX();
        int length = e.getY();
        
        if (x < 0 || y < 0) {
            x = -x;
            y = -y;
        }
        
        int diameter = width - x ^ 2 + length - y ^ 2;
        double pi = Math.PI;
        
        switch (form) {
        case 'r':
            if (x < 0) {
                x = x * (-1);
            }
            Rectangle rect = new Rectangle(x, y, width - x, length - y);
            //STUB-Klasse zum TESTEN OB DIE METHODE RICHTIG FUNKTIONIERT
            //StubRectangle stubRect = new StubRectangle(x, y, width, length);
            //drawing.addFigures(stubRect);
            drawing.addFigures(rect);
            repaint();
            rectangles = true;
            //System.out.println(Check());
            break;
            
        
        case 'c':
            Circle circ = new Circle(x, y, (int) (pi) * diameter);
            drawing.addFigures(circ);
            repaint();
            circles = true;
            //System.out.println(Check());
            break;
        
        case 'l':
            Line line = new Line(x, y, width, length);
            drawing.addFigures(line);
            repaint();
            lines = true;
            //System.out.println(Check());
            break;
        }
    }

// 5 Sprachelemente
    @objid ("c78a1cc3-967a-4afc-b0d2-f0beccb9ff25")
    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @objid ("1e12ee00-870f-4e73-951d-3412da740542")
    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @objid ("27968f01-94ed-4269-b6fe-35e468a950c7")
    @Override
    public void mouseExited(MouseEvent e) {
    }

    @objid ("178f0f09-a03b-4ecb-922b-19af3c1e9bfa")
    @Override
    public void keyReleased(KeyEvent e) {
    }

    @objid ("d2cb0ea4-04be-49e7-a4a2-98656e57062f")
    @Override
    public void keyTyped(KeyEvent e) {
    }

    @objid ("90633fad-cb8f-4de7-8d10-e4e2cd13a290")
    @Override
    public void keyPressed(KeyEvent e) {
        form = e.getKeyChar();
    }

    @objid ("7f846f12-8313-4631-84d8-17cc680d24a8")
    @Override
    public void paintComponent(final Graphics g) {
        super.paintComponent(g);
        if (drawing != null){
            drawing.drawFigure(g);
        
        }
    }

    @objid ("dbe57b0f-853c-4d3b-93c2-a6803afdebc3")
    public void setDrawing(final Drawing drawing) {
        this.drawing = drawing;
    }

//Interface
    @objid ("fb9d3107-cfd8-4cc5-a943-7ba6769ed043")
    public String Check() {
        if (rectangles==true){
            return "is a rectangle";
        }
        if(lines==true){
            return "is a line";
        }
        if(circles==true){
            return "is a circle";
            
        }
        else{
        return "NOT DEFINED";
        }
    }

}
