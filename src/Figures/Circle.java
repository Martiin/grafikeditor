package Figures;

import java.awt.Graphics;
import com.modeliosoft.modelio.javadesigner.annotations.objid;

@objid ("b45a9ee3-30d8-4930-b539-926fc6536887")
public class Circle extends Figure {
    @objid ("edae2264-0b93-48d2-a387-da79107fd612")
     int r;

    @objid ("b7c31e48-2a72-494d-ba23-a5de2a857c3a")
    Circle(int x, int y, int r) {
        super(x, y);
        this.r = r;
    }

    @objid ("aa45f94f-05a4-46be-b714-b452f7981329")
    public void draw(Graphics g) {
        g.drawOval(x - r, y - r, 2 * r, 2 * r);
    }

}
