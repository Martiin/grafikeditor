package Figures;

import java.awt.Graphics;
import com.modeliosoft.modelio.javadesigner.annotations.objid;

@objid ("1c4c8de1-56ac-4d5b-9d5a-033d8aa38e0f")
public abstract class Figure {
    @objid ("c168a7e7-1094-4479-b20b-e84c3c8a7a9e")
     int x;

    @objid ("ca3bb228-eda7-453d-bfcb-01bb6a0f74db")
     int y;

    @objid ("82fe97da-53c1-4689-bbbc-057f9f864b75")
    Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @objid ("f31e8a1f-806b-4f32-ab3f-7ba49fc80e07")
    public abstract void draw(Graphics g);

}
