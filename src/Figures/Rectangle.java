package Figures;

import java.awt.Graphics;
import com.modeliosoft.modelio.javadesigner.annotations.objid;

@objid ("7ea0a316-d1a3-4250-a547-6fd5204db04b")
public class Rectangle extends Figure {
    @objid ("edca9b28-50b2-4722-b072-4b403c6fb119")
     int width;

    @objid ("93a1d86c-348d-49fe-bb39-80546a96bd71")
     int length;

    @objid ("66c57e1a-b9ad-4542-b41f-543063750f90")
     Graphics g;

    @objid ("db4abff0-87d7-4f1f-973c-046b03978c23")
    Rectangle(int x, int y, int width, int length) {
        super(x, y);
        this.width = width;
        this.length = length;
    }

    @objid ("13bb6297-63dc-4580-9540-190989f7f6f9")
    @Override
    public void draw(Graphics g) {
        g.drawRect(x, y, width, length);
    }

}
