package Figures;

import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import com.modeliosoft.modelio.javadesigner.annotations.objid;

@objid ("86926838-f1d4-45ad-9236-dc45a2391ab6")
public class Display extends JFrame {
    @objid ("4addc535-cd70-44fa-aa76-9f75ddc16931")
    private static final long serialVersionUID = 1L;

    @objid ("a5e131ad-1cf6-4e54-9a3c-bc6e5fa4f8e0")
     static Paper paper = new Paper();

//Java-Klasse
    @objid ("fea85987-c86e-4699-b488-cbf8cd8b03e5")
     static ArrayList<Shape> shapes = new ArrayList<Shape>();

    @objid ("3aea2bbe-1a5b-4bf8-ad03-7f4d17babbb2")
     static JButton clear = new JButton();

    @objid ("b00e900b-dbb1-486e-bb9b-4bb6a59b046c")
    public static void program() {
        JFrame frame = new JFrame("Graphic Editor");
        JButton rectangle = new JButton("Rectangle");
        JButton circle = new JButton("Circle");
        JButton line = new JButton("Line");
        
        
        
        
        // JFrame
        frame.setVisible(true);
        frame.setSize(800, 800);
        frame.setContentPane(paper);
        
        
        // paper
        clear.setVisible(true);
        paper.add(rectangle);
        paper.add(circle);
        paper.add(line);
        paper.add(clear);
        clear.setText("Clear");
        
        
        // 3 Sprachelemente
        
        
        /*clear.addActionListener((new ActionListener () -> {
            if (e.getSource() == clear) {
                frame.clear();                
            }
        });*/
        
        
        /*clear.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == clear) {
                    rectangle.removeAll();
                    circle.removeAll();
                    line.removeAll();
                    frame.repaint();
                }
            }
        
        });*/
        
        /*clear.addActionListener(ActionListener e)->{
            Drawing.figures.removeAllElements();
            Paper.g.clearRect(0,0, Paper.paintImage.getX()), Paper.paintImage.getY();
                
            }*/
        
        
        
        
        rectangle.addActionListener((ActionEvent e) -> {
            Paper.form = 'r';
        });
        
        circle.addActionListener((ActionEvent e) -> {
            Paper.form = 'c';
        });
        
        line.addActionListener((ActionEvent e) -> {
            Paper.form = 'l';
        });
    }

    @objid ("f49e1010-00a6-4792-a8b2-5c49f52aa2d9")
    public Display() {
        super("GraphicEditor fuer Figuren");
    }

    @objid ("b85457c9-2fe3-44e2-aedc-766af9cd9c3c")
    public void show(Drawing drawing) {
        paper.setDrawing(drawing);
    }

}
