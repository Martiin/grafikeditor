package Figures;

import java.awt.Graphics;
import com.modeliosoft.modelio.javadesigner.annotations.objid;

@objid ("cdbfd5f4-546e-4c8c-bc41-4eeb09e363c1")
public class Line extends Figure {
    @objid ("1dd7e5e0-ca2e-4223-b01e-84b39c5ff6bf")
     int posX;

    @objid ("c9157f07-9015-4e6e-9bd7-630d39b45e99")
     int posY;

    @objid ("440f7ca2-ff01-4b0f-bbc5-3de284cf8367")
     int length;

    @objid ("70de50bb-3a34-4e11-bf91-da570be66640")
    public Line(int x, int y, int x1, int y1) {
        super(x, y);
        this.posX = x1;
        this.posY = y1;
    }

    @objid ("ec7a2f28-efc3-4c4a-9dd6-7811d1c85be7")
    @Override
    public void draw(Graphics g) {
        g.drawLine(x, y, posX, posY);
    }

}
