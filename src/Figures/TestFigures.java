package Figures;

import com.modeliosoft.modelio.javadesigner.annotations.objid;

@objid ("3e9d47a0-d159-4686-b5cf-b942b0a94676")
public class TestFigures {
    @objid ("9a09898d-408b-45c9-aa39-3eb93112b87f")
    public static void main(String[] args) {
        Figure[] List = new Figure[4];
        
        Rectangle r = new Rectangle(10, 10, 20, 80);
        List[0] = r;
        Circle c = new Circle(10, 10, 20);
        List[1] = c;
        Line l = new Line(10, 10, 20, 20);
        List[2] = l;
        
        Drawing d;
        d = new Drawing(List);
        Display display = new Display();
        d.addFigures(new Rectangle(100, 100, 200, 300));
        d.addFigures(new Circle(200, 200, 50));
        d.addFigures(new Line(200, 200, 200, 300));
        d.addFigures(new Line(20, 200, 200, 300));
        Display.program();
        display.show(d);
    }

}
