package Figures;

import java.awt.Graphics;
import java.util.Vector;
import com.modeliosoft.modelio.javadesigner.annotations.objid;

// Verkettete Liste
@objid ("f3130b22-5b6d-4f72-9d01-9d9164239d77")
class Drawing {
    @objid ("837faf94-38ba-4811-807f-d7d052fe43a2")
    public static Vector<Figure> figures = new Vector<Figure>();

    @objid ("d2bfb003-2113-43ea-9f27-5e1118e47f3d")
    public static Graphics g;

    @objid ("0d092b60-d995-4129-b7f3-2d6f70bda705")
    public void drawFigure(Graphics g) {
        for (Figure f : figures) {
            f.draw(g);
        }
    }

    @objid ("2ac9778b-55cf-435f-9153-777984290814")
    public Drawing(Figure[] figuren) {
    }

    @objid ("f4c50faa-ec06-4da3-bea9-c9741a37be9b")
    public void addFigures(Figure f) {
        figures.addElement(f);
    }

}
